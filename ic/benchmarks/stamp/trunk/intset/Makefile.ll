TMBUILD ?= seq

PROG := intset-ll

SRCS += \
	intset.c

OBJS := ${SRCS:.c=.o}

# TODO: remove ggdb3 
# TODO: Nilson
# CFLAGS += -DUSE_LINKEDLIST -ggdb3
CFLAGS += -DUSE_LINKEDLIST
include ../common/$(TMBUILD)/Makefile.common


