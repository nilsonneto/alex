#!/bin/bash

app="intset-ll"
work="read"
appMod="$app.$work"
numRep=1
echo "$appMod"

./execute.sh -a $appMod -n $numRep
./gen-data.sh -n $numRep -d $app$work -s 'glibc hoard tbbmalloc tcmalloc' -c $appMod':tinySTM:suicide:'
./plot-graph.sh -d $app$work -n $numRep -a $app -k read -g bar-intset.gnu
