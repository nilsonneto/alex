Transactional Memory and Memory Allocators Interaction Study
===============

Dependences
-----------

The scripts used to compile, run, and post-process the data rely on some
linux-based tools. In general they should be available in a typical distro, but
we are listing here the main ones. In case some of the scripts finish with a
weird error message, this could be of help.

* Gnuplot
* R Project - See http://www.r-project.org
* Ubuntu Packages
  * automake1.13
    * Download `http://ftp.gnu.org/gnu/automake/automake-1.13.4.tar.xz`
    * Run `./configure;make;sudo make install`
  * Run `sudo apt-get install automake automake1.11 llvm llvm-dev llvm-runtime libpapi5.4.3 libpapi-dev pfm libpfm4 libpfm4-dev`

You should also check the dependences of each of the tools (allocators, STM,
etc.) before compiling them.

Quick Start
-----------

The first thing you should do is compile the packages needed to execute the
experiments.  This includes the allocators, tools, TM libraries and, finally,
benchmarks. We provide a simple bash script to compile each of them (please
enter their respective directories before executing the commands):

* Allocators:	`./gen-allocators.sh`
* Tools (needed if cache miss data is required): `./gen-tools.sh`
* Run `sudo ln -s [PROJ_HOME]/tools/malloc_profile/libmallocprof.so /usr/lib/libmallocprof.s`
* Threadtest: `make`
* STAMP and Intset: `./compile-all.sh`

If all succeeds you are ready to run the experiments. Most of the benchmarks
have a script to do it. You might want to look in the
`benchmarks/stamp/scripts/` directory.  The main script is `execute.sh`,
followed by `gen-data.sh` and `plot-graph.sh`.  They currently have a very
limited but somewhat useful command-line help system.  For instance, to
execute the `yada` application 5 times you should run:

`./execute.sh -a "yada" -n 5`

This will run the application with the default STM library (tinySTM), with four
allocators (glibc, hoard, tbbmalloc, and tcmalloc), 1, 2, 4, and 8 threads,
five times each. Try using `./execute.sh -h` for some help.  The execution
script will create a subdirectory named `results` (default) in the stamp
directory to save all runs.

To post-process the data and generate the chart for the previous executions you 
could try the following:

`./plot-graph.sh -d stamp -g bar-stamp.gnu -a "yada"`

It will store the tables and chart inside a directory named `stamp` (flag
`-d`).  The root directory for tables and charts is stored in the
`scripts.cfg` file, variable `TABDIR`. This file also holds general
configuration options used by the scripts.  The `plot-graph.sh` script relies
on the `gnuplot` script (flag `-g`) to plot the graph. Currently, the ones
provided (extension `.gnu`) only support 8 threads, but it should not be hard
to change them to your needs.


Please keep in mind that this is working-in-progress and any help is really
welcome.  Also, take a look at our [artifact
page](http://lampiao.lsc.ic.unicamp.br/~baldas/artifact/ppopp15-artifact.html)
for further information.



Known Issues
---------------

* When compiling the version of gperftools provided, you might run into a
configuration issue due to a different version of automake. You can try to
download and perform a local installation of automake 1.13 to solve the issue.

* Only if you need PAPI and malloc count support: You need to manually set up
  the `BASEDIR` variable in the
`tm-study-malloc/benchmarks/stamp/trunk/common/Makefile.common` file with the
absolute path of your base directory.

